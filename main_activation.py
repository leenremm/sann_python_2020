from datetime import datetime
import os
os.system('cls')

import pprint
import numpy as np
pp = pprint.PrettyPrinter(indent=4)
import base64
import copy
import pandas as pd

# =====================================================================================

from functions import *

# =====================================================================================

cls()

# Initialize datasets
load_dataset()

# Load pre-processing autoencoder
load_autoencoder()
evaluate_autoencoder()
log("Step 1a: Initialize pre-processing (autoencoder)")

# Initialize SANN
init_sann()
visualize_sann("001", "Step 1b: SANN initialized (random weights)")

'''
# ==============================================================================
# Training (without salience)
#max_steps = 101   # Without salience
max_steps = 72    # With salience

step = 5
train_sann(epochs=step)
class_score, individual_score, class_confs, individual_confs = evaluate_sann()
print ("Epoch: %3d, Class: %d/12, Individual: %d/12, Class confs: %s, Individual confs: %s" % (step, class_score, individual_score, np.round(class_confs,3), np.round(individual_confs,3)))

for i in range (2,max_steps):
    train_sann(epochs=step)
    class_score, individual_score, class_confs, individual_confs = evaluate_sann()
    print ("Epoch: %3d, Class: %d/12, Individual: %d/12, Class confs: %s, Individual confs: %s" % (step*i, class_score, individual_score, np.round(class_confs,3), np.round(individual_confs,3)))
# ==============================================================================
'''

load_sann()
class_score, individual_score, class_confs, individual_confs = evaluate_sann(verbose=True)
#show_box_plot(class_confs, individual_confs)
print ("\n%s\nBefore Salience training:" % ("="*100))
print ("\nClass Confidence:")
print (", ".join(map(str, class_confs)))
print ("\nIndividual Confidence:")
print (", ".join(map(str, individual_confs)))
show_box_plot(class_confs, individual_confs)

# ==============================================================================

print ("\n%s\nRunning simulation across each element of the dataset:" % ("="*100))
individual_with_confs = []
individual_without_confs = []
class_with_confs = []
class_without_confs = []

for image_i in range(0,12):

    log("Reloading SANN. One time salience training on image: %2d" % image_i)

    load_sann()
    evaluate_sann()
    visualize_sann("002", "Step 2: SANN trained for classification")

    # Set activation function impact:
    # Options:  None, "gradient", "offset_positive", "offset_negative", "amplitude"
    sann_set_variation(variation=None)
    sann_set_variation(variation="gradient")
    sann_set_variation(variation="offset_positive")
    sann_set_variation(variation="amplitude")

    # Positive Salience Training (dog)
    salience_train_sann(image_key=image_i, salience_value=1)
    evaluate_sann()
    visualize_sann("003", "Step 3: Negative salience training (image #9, class: dog)")

    class_score, individual_score, class_confs, individual_confs = evaluate_sann(verbose=False)

    # Append: with salience
    individual_with_confs = np.concatenate((individual_with_confs, [x for i,x in enumerate(individual_confs) if i==image_i]))
    class_with_confs = np.concatenate((class_with_confs, [x for i,x in enumerate(class_confs) if int(i/4)==int(image_i/4)]))

    # Append: without salience
    '''
    individual_without_confs = np.concatenate((individual_without_confs, [x for i,x in enumerate(individual_confs) if i!=image_i]))
    class_without_confs = np.concatenate((class_without_confs, [x for i,x in enumerate(class_confs) if int(i/4)!=int(image_i/4)]))
    '''
    
# Box Plot: with salience
print ("\n%s\nAfter Salience Training (images with salience tag):" % ("="*100))
print ("\nClass Confidence:")
print (", ".join(map(str, class_with_confs)))
print ("\nIndividual Confidence:")
print (", ".join(map(str, individual_with_confs)))
show_box_plot(class_with_confs, individual_with_confs)

# Box Plot: without salience
'''
print ("\n%s\nAfter Salience Training (images without tag):" % ("="*100))
print ("\nClass Confidence:")
print (", ".join(map(str, class_without_confs)))
print ("\nIndividual Confidence:")
print (", ".join(map(str, individual_without_confs)))
show_box_plot(class_without_confs, individual_without_confs)
'''

import pdb; pdb.set_trace()

# =====================================================================================
