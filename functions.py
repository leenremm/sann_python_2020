# ==============================================================================
# Common functions
# ==============================================================================

import numpy as np
np.random.seed(0)
import pickle
import json
import cv2
import requests
from datetime import datetime, timedelta
from colorama import init; init(); from colorama import Fore, Back, Style
import matplotlib.pyplot as plt
import base64
import io
import glob
import pandas as pd

from SANN import *

import os
os.environ["KERAS_BACKEND"] = "theano"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from keras.layers import Input, Dense
from keras.models import Sequential, Model
from keras.datasets import fashion_mnist, mnist
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint, EarlyStopping

import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

import warnings
warnings.simplefilter(action='ignore')

# ==============================================================================

host = '127.0.0.1'

ports = {
    "arousal":  5000,
    "thalamus": 5010,
    "cortex":   5020,
}

# ==============================================================================

def nothing(x): pass

# ==============================================================================

def b64_encode(img_array=[]):
    img_b64 = base64.urlsafe_b64encode(np.round(img_array,2))
    img_b64_decoded = img_b64.decode('utf-8')
    return img_b64_decoded

# ==============================================================================

def b64_decode(str_b64_encoded=""):
    img_b64_encoded = str_b64_encoded.encode('utf-8')
    return np.frombuffer(base64.urlsafe_b64decode(img_b64_encoded), np.float32)

# ==============================================================================

def empty_img(img_dim=28):
    return b64_encode(np.zeros((img_dim,img_dim), dtype=np.float32))

# ==============================================================================

def log(message, level="info", color=""):
    message_clean = str(message).strip()
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    colour_codes = {
    "INFO": Fore.WHITE,
    "ERR.": Fore.RED,
    "WARN": Fore.YELLOW,
    "PASS": Fore.GREEN,
    "BLUE": Fore.BLUE}
    print_str = Fore.WHITE + ("%s" % timestamp)
    if len(color) == 0:
        color = level
    print_str += " [" + colour_codes[color.upper()] + ("%4s" % level.upper()) + Fore.WHITE + "]"
    print_str += Fore.WHITE + ": " + colour_codes[color.upper()] + ("%s" % message_clean) + Fore.WHITE
    print(print_str)

# ==============================================================================

def cleanup_log_array(arr, secs=5):
    return [i for i in arr if i[0] >= (datetime.now()-timedelta(seconds=secs))]

# ==============================================================================

def get_current_state(port_name="arousal"):
    response = requests.get("http://%s:%s/current_state" % (host, ports[port_name]))
    return response.json()

# ==============================================================================

def send_to(port_name="cortex", img=empty_img()):
    img_b64_decoded = b64_encode(img)
    response = requests.get("http://%s:%s/process_image?img_b64_decoded=%s" % (host, ports[port_name], img_b64_decoded))
    state = response.json()
    return state

# ==============================================================================

def trigger_chemical_release(chemical="noradrenaline", value=0):
    requests.get("http://%s:%s/add?chemical=%s&value=%s" % (host, ports["arousal"], chemical, value))
    return True

# ==============================================================================

def show_image(img):
    plt.imshow(img, cmap='Greys_r', vmin=0, vmax=1)
    plt.axis('off')
    plt.show()

# ==============================================================================

def gui_bars(num, max_bars=100, bar_width=90):
    max_bars = max(num, max_bars)
    percent = num / max_bars
    bars = int(np.ceil(percent * bar_width))
    output_str = "[" + ("|" * bars) + (" "*(bar_width-bars)) + "]"
    return output_str

# ==============================================================================

def rmsdiff(im1, im2):
    """Calculates the root mean square error (RSME) between two images"""
    return np.sqrt(np.mean((im1-im2)**2))

# ==============================================================================

def rmsdiff_thalamus(input, prediction):

    # Construct Difference
    difference = input-prediction

    # Remove negative numbers
    min_zero = np.where(difference<0, 0, difference)

    # Calculate RMS
    return np.sqrt(np.mean((min_zero)**2))

# ==============================================================================

def load_data():

    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

    # Normalize all values between 0 and 1
    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.

    # Flatten the 28x28 images into vectors of size 784
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    return x_train, x_test

# ==============================================================================

def init_nn(input_dim=784, encoding_dim=16):

    autoencoder = Sequential()
    autoencoder.add(Dense(1024, activation='elu', input_shape=(input_dim,)))
    autoencoder.add(Dense(512, activation='elu'))
    autoencoder.add(Dense(encoding_dim, activation='elu', name="encoded"))
    autoencoder.add(Dense(512, activation='elu'))
    autoencoder.add(Dense(1024, activation='elu'))
    autoencoder.add(Dense(input_dim, activation='sigmoid'))
    autoencoder.compile(loss='mean_squared_error', optimizer = Adam(), metrics=['accuracy'])

    # Encoder Model
    encoder = Model(autoencoder.input, autoencoder.get_layer('encoded').output)

    # Decoder Model
    encoded_input = Input(shape=(encoding_dim,))
    decoder = autoencoder.layers[-3](encoded_input)
    decoder = autoencoder.layers[-2](decoder)
    decoder = autoencoder.layers[-1](decoder)
    decoder = Model(encoded_input, decoder)

    encoder._make_predict_function()
    decoder._make_predict_function()

    return autoencoder, encoder, decoder

# ==============================================================================

def train_nn(autoencoder, x_train, x_test, epochs_n=20):

    # Save H5 files at checkpoints
    #filepath="saved_nn_models\\weights-{epoch:02d}-{val_loss:.4f}.h5"
    #checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=0, save_best_only=True, mode='min')
    #callbacks_list = [checkpoint]

    # Early Stopping
    #callbacks_list = [EarlyStopping(monitor = 'val_loss', min_delta = 0, patience = 10, verbose = True, mode = 'auto')]

    history = autoencoder.fit(  x_train,
                                x_train,
                                epochs=epochs_n,
                                batch_size=1,
                                shuffle=True,
                                verbose=1,
                                validation_data=(x_test, x_test),)
                                #callbacks=callbacks_list)

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('autoencoder loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['training data loss', 'validation data loss'], loc='upper left')
    plt.show()

    return autoencoder, history

# ==============================================================================

def reconstruct(encoder, decoder, x_test):

    encoded_imgs = encoder.predict(x_test)
    decoded_imgs = decoder.predict(encoded_imgs)

    return encoded_imgs, decoded_imgs

# ==============================================================================

def classify_from_encoding(sann, img_small, dim=4, output_layer="sigmoid"):

    img_solve = sann.solve(np.array([img_small]))

    # Softmax implementation (sum to 1)
    if (output_layer == "softmax"):
        solve_sum = sum(sum(img_solve))
        img_solve = img_solve/solve_sum

    class_key = np.argmax(img_solve[0][0:3])
    class_conf = img_solve[0][0:3][class_key]

    individual_key = np.argmax(img_solve[0][3:])
    individual_conf = img_solve[0][3:][individual_key]

    reverse_salience_value = sann.calculate_reverse_salience(img_small.reshape(dim*dim))
    #reverse_salience_value = 0

    return class_key, class_conf, individual_key, individual_conf, reverse_salience_value

# ==============================================================================

def lookup_label(class_key):

    labels = [
        "bird",
        "cat",
        "dog"
    ]

    try:
        class_key = min(len(labels), max(0,int(class_key)))
        label = labels[class_key]
    except:
        label = "None"

    return label

# ==============================================================================

def save_nn_to_file(model, name="nn"):

    # Save model
    json_filename = "saved_nn_models\\model_" + name + ".json"
    with open(json_filename, "w") as json_file:
        json_file.write(model.to_json())
    json_file.close()

    # Save weights
    h5_filename = "saved_nn_models\\model_" + name + ".h5"
    model.save_weights(h5_filename)

    return True

# ==============================================================================

def load_nn_from_file(model, name="nn"):

    # Load JSON
    '''
    json_filename = "saved_nn_models\\model_" + name + ".json"
    with open(json_filename) as file_handle:
        model = model_from_json(file_handle.read())
    '''

    # Load weights
    h5_filename = "saved_nn_models\\model_" + name + ".h5"
    model.load_weights(h5_filename)

    return model

# ==============================================================================

# define a function which returns an image as numpy array from figure
# https://stackoverflow.com/questions/7821518/matplotlib-save-plot-to-numpy-array

def get_img_from_fig(fig, dpi=180):
    buf = io.BytesIO()
    fig.savefig(buf, format="png", dpi=dpi)
    buf.seek(0)
    img_arr = np.frombuffer(buf.getvalue(), dtype=np.uint8)
    buf.close()
    img = cv2.imdecode(img_arr, 1)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img

# ==============================================================================

def open_image(filename):

    # Opens the file
    if not(os.path.isfile(filename)):
        return False, None # Exception if file does not exist
    try:
        img = cv2.imread(filename)
    except:
        return False, None # Exception if file cannot open
    return True, img

# ==============================================================================

def load_training_images(training_dir="dataset/*/*.*", img_dim=28, log_polar_transform=False, log_polar_res=5.0, cropping=False):

    # Training images
    images = []
    labels = []
    filename_array = glob.glob(training_dir)

    file_count = 1
    for filename in filename_array:
        status, new_file = open_image(filename)
        img = grayscale(new_file)
        if cropping == True:
            img_h, img_w = img.shape
            img_left = int(img_w/2 - img_h/2)
            img_right = img_left + img_h
            img = img[0:img_h, img_left:img_right]
        img = cv2.resize(img, (img_dim, img_dim))
        images.append(img)
        label = filename.split("\\")[-2]
        # Class
        class_label = categorical_converter(int(label)+1, 3)
        # Individual
        individual_label = categorical_converter(file_count, len(filename_array))
        # Combined
        combined_label = class_label+individual_label
        combined_label = [char for char in combined_label]
        labels.append(combined_label)
        file_count += 1

    images = np.array(images)
    labels = np.asarray(labels, dtype=np.float32)

    # Normalize all values between 0 and 1
    images = images.astype('float32') / 255.

    # Inverse Images
    images = 1 - images

    # Transform (log polar)
    if (log_polar_transform == True):
        images = transform_dataset(images, 0, 1, log_polar_res=log_polar_res)

    # Flatten the 28x28 images into vectors of size 784
    images_flat = images.reshape((len(images), np.prod(images.shape[1:])))

    return images_flat, labels

# ==============================================================================

def categorical_converter(integer, digits):
    output = ["0"]*digits
    output[integer-1] = "1"
    return "".join(output)

# ==============================================================================

def grayscale(img_original):

    img = img_original.copy()

    # Converts colour image to grayscale
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return img

# ==============================================================================

def transform_dataset(data, rotate_degrees=0, resize_percent=1, log_polar_res=5.0):

    x_data = []

    for i in range(0, len(data)):

        i_data = transform_logpolar(data[i], log_polar_res=log_polar_res)
        x_data.append(i_data)

    return np.array(x_data)

# ==============================================================================

def transform_logpolar(img, log_polar_res=5.0):

    value = np.sqrt(((img.shape[1]/log_polar_res)**2.0)+((img.shape[0]/log_polar_res)**2.0))
    log_polar_image = cv2.logPolar(img,(img.shape[1]/2, img.shape[0]/2), value, cv2.WARP_FILL_OUTLIERS)
    return log_polar_image

# ==============================================================================

def transform_logpolar_inv(img, log_polar_res=5.0):

    value = np.sqrt(((img.shape[1]/log_polar_res)**2.0)+((img.shape[0]/log_polar_res)**2.0))
    inv_log_polar_image = cv2.logPolar(img, (img.shape[1]/2, img.shape[0]/2), value, cv2.WARP_FILL_OUTLIERS + cv2.WARP_INVERSE_MAP)
    return inv_log_polar_image

# =====================================================================================

def datetimeconverter(o):
    if isinstance(o, datetime):
        return o.__str__()

# =====================================================================================

def return_json(dict_to_return):
    return json.dumps(dict_to_return, default=datetimeconverter)

# =====================================================================================

def generate_gif(path="sann_gif", duration=200):

    import glob
    from PIL import Image

    # filepaths
    fp_in = "img/%s/*.jpg"%path
    fp_out = "img/%s/output.gif"%path

    # https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#gif
    img, *imgs = [Image.open(f) for f in sorted(glob.glob(fp_in))]
    img.save(fp=fp_out, format='GIF', append_images=imgs, save_all=True, duration=duration, loop=0)

# =====================================================================================


def update_request_load():
    global state, request_history
    request_history = cleanup_log_array(request_history)
    state["request_load"] = len(request_history)

# =====================================================================================

def classify_image(img):

    global state, autoencoder, encoder, decoder, sann, x_train, x_test, request_history

    # Timer
    startTime = scriptTimer(startFlag=True)

    # Get Encoding
    encoded_imgs, decoded_imgs = reconstruct(encoder, decoder, [[img]])

    # Classify
    class_key, class_conf, individual_key, individual_conf, reverse_salience_value = classify_from_encoding(sann, encoded_imgs[0], dim=4)

    class_name = lookup_label(class_key)

    # Timer
    diffTime = scriptTimer(endFlag=True, startTime=startTime)

    return class_key, class_name, class_conf, individual_key, individual_conf, reverse_salience_value

# =====================================================================================

def train_autoencoder(epochs=100):

    global state, autoencoder, encoder, decoder, x_train, y_train, x_encoded

    # Initialize NN
    log ("Initializing Autoencoder models...")
    autoencoder, encoder, decoder = init_nn(input_dim=len(x_train[0]), encoding_dim=16)

    log ("Training Autoencoder...")
    autoencoder, history = train_nn(autoencoder, x_train, x_train, epochs_n=epochs)

    log ("Saving Autoencoder models...")
    save_nn_to_file(autoencoder, "autoencoder")

    log ("Generating encoded values...")
    x_encoded, _ = reconstruct(encoder, decoder, x_train)

    log ("Training complete")

    return return_json({"status": "success", "message" : "Training complete"})

# =====================================================================================

def evaluate_autoencoder():

    global state, autoencoder, encoder, decoder, x_train, x_encoded

    try:

        log ("Evaluating dataset...")
        x_encoded, decoded_imgs = reconstruct(encoder, decoder, x_train)
        log ("Showing original image...")
        original_image = x_train[8].reshape((img_dim,img_dim))
        show_image(original_image)
        log ("Showing encoding...")
        show_image(x_encoded[8].reshape((4,4)))
        log ("Showing decoded image...")
        decoded_image = decoded_imgs[8].reshape((img_dim,img_dim))
        show_image(decoded_image)
        log ("Calculating accuracy...")
        pixelwise_error = sum(sum(abs(decoded_image - original_image))) / sum(sum(original_image))
        pixelwise_accuracy = 100 * (1 - pixelwise_error)
        log ("Pixelwise accuracy (image #8): %.2f%%" % pixelwise_accuracy)
        log ("Evaluation complete")

    except:

        log("Failed to evaluate autoencoder model. Try /train or /load first.")
        return return_json({"status": "error", "message" : "Failed to evaluate model. Try /train or /load first."})

    return return_json({"status": "success", "message" : "Evaluation complete. Pixelwise accuracy (image #8): %.2f%%" % pixelwise_accuracy})

# =====================================================================================

def load_dataset():

    global x_train, y_train

    try:

        log ("Loading dataset...")
        x_train, y_train = load_training_images(training_dir="dataset/animals/*/*.*", img_dim=img_dim, cropping=True)

    except:

        log("Failed to load dataset.")
        return return_json({"status": "error", "message" : "Failed to load dataset."})

    return return_json({"status": "success", "message" : "Loading dataset complete"})

# =====================================================================================

def load_autoencoder():

    global state, autoencoder, encoder, decoder, x_train, y_train, x_encoded

    try:

        # Initialize NN
        log ("Initializing Autoencoder model...")
        autoencoder, encoder, decoder = init_nn(input_dim = len(x_train[0]), encoding_dim=16)

        log ("Loading Autoencoder weights...")
        autoencoder = load_nn_from_file(autoencoder, "autoencoder")

        log ("Loading encoded values of dataset...")
        x_encoded, _ = reconstruct(encoder, decoder, x_train)

    except:

        log("Failed to load Autoencoder model from file. Try /train first.")
        return return_json({"status": "error", "message" : "Failed to load Autoencoder model from file. Try /train first."})

    return return_json({"status": "success", "message" : "Loading autoencoder model complete"})

# =====================================================================================

def init_sann():

    global state, sann, x_train, y_train, x_encoded, salience_value

    # Initialize NN
    log ("Initializing SANN model...")
    input_dim = int(np.sqrt(x_encoded.shape[1]))
    output_dim = y_train.shape[1]
    nn_dim = [input_dim*input_dim, input_dim*input_dim, output_dim]
    sann = SANN(nn_dim)
    sann.set_activation_function(sann.activation_sigmoid)
    sann.init_method()

    try:

        pass

    except:

        log("Failed to initialize SANN model")
        return False

    log("SANN initialization complete")
    return True

# =====================================================================================

def load_sann():

    global state, sann, x_train, y_train, x_encoded, salience_value

    try:

        log ("Loading SANN weights (.bin files)...")
        sann_filename = "saved_nn_models/model_sann.bin"
        sann = SANN.load_from_bin(sann_filename)

    except:

        log("Failed to load SANN model from file")
        return False

    log("SANN load complete")
    return True

# =====================================================================================

def sann_set_variation(variation=None):

    global sann
    sann.variation = variation
    print ("SANN activation variation: %s" % sann.variation)

# =====================================================================================

def train_sann(epochs=200):

    global state, sann, x_train, y_train, x_encoded

    #log ("Training SANN...")
    sann.initialize_salience()
    sann.train(x_encoded, y_train, x_encoded, y_train, epochs=epochs)

    #log ("Saving SANN model (bin format)...")
    sann_filename = "saved_nn_models/model_sann.bin"
    sann.save_to_bin(sann_filename)

    #log ("Training complete")

    return return_json({"status": "success", "message" : "SANN initialization complete"})

# =====================================================================================

def salience_train_sann(image_key=0, salience_value=0):

    global state, sann, x_encoded

    try:

        log ("Tagging image #%d with salience value %d..." % (image_key, salience_value), color="PASS")
        input_dim = int(np.sqrt(x_encoded.shape[1]))
        sann.salience_train_activation(x_encoded[image_key].reshape(input_dim*input_dim), salience_value=salience_value)

    except:

        log("Failed to load SANN model from file.")
        return False

    log("SANN salience training complete")
    return True

# =====================================================================================

def salience_update_weights_sann(image_key=0):

    global state, sann, x_encoded

    try:

        log ("Updating SANN weights...")
        input_dim = int(np.sqrt(x_encoded.shape[1]))
        sann.salience_update_weights(x_encoded[image_key].reshape(input_dim*input_dim))

    except:

        log("ERROR: Failed to update SANN weights!")
        return False

    log("SANN weight update complete")
    return True

# =====================================================================================

def evaluate_reverse_salience_range():

    global x_train

    # Evaluate Images
    reverse_salience_list = []

    for img in x_train:

        class_key, class_name, class_conf, individual_key, individual_conf, reverse_salience_value = classify_image(img)
        reverse_salience_list.append(reverse_salience_value)

    log ("Reverse Salience Analysis: ")
    log ("Min: %1.4f, Max: %1.4f" % (min(reverse_salience_list), max(reverse_salience_list)))

# =====================================================================================

def evaluate_sann(verbose=False):

    #evaluate_reverse_salience_range()

    global x_train

    # Evaluation Arrays
    class_mark = []
    individual_mark = []
    class_confs = []
    individual_confs = []

    # Evaluate Images
    if verbose:
        print()
    for i in range(len(x_train)):

        img = x_train[i]

        class_key, class_name, class_conf, individual_key, individual_conf, reverse_salience_value = classify_image(img)

        #print (i+1, individual_key+1, (individual_key+1 == i+1), int(i/4)+1, class_key+1, (class_key+1 == int(i/4)+1))
        individual_mark.append((individual_key+1 == i+1))
        individual_confs.append(individual_conf)
        class_mark.append((class_key+1 == int(i/4)+1))
        class_confs.append(class_conf)

        # Calculate Action
        action = "neutral"
        if (reverse_salience_value > 0.02):
            action = "positive (attraction)"
        elif (reverse_salience_value < -0.02):
            action = "negative (fear)"

        if verbose:
            print ("    Image # %2d => Class name: %5s (%3.1f%%), Individual key: %2d (%3.1f%%), Salience: %+.3f, Action: %s " % (i+1, class_name, class_conf*100, individual_key+1, individual_conf*100, (reverse_salience_value or 0), action))

    if verbose:
        print()

    return sum(class_mark), sum(individual_mark), class_confs, individual_confs

# =====================================================================================

def show_box_plot(class_confs, individual_confs):

    # http://blog.bharatbhole.com/creating-boxplots-with-matplotlib/
    data_to_plot = [class_confs, individual_confs]
    fig = plt.figure(1, figsize=(10, 7))
    ax = fig.add_subplot(111)
    bp = ax.boxplot(data_to_plot)
    labels = ['Class', 'Individual']
    print(get_box_plot_data(labels, bp))
    plt.xticks([1, 2], labels)
    fig.show(bp)

def get_box_plot_data(labels, bp):
    # https://stackoverflow.com/questions/23461713/obtaining-values-used-in-boxplot-using-python-and-matplotlib
    rows_list = []
    for i in range(len(labels)):
        dict1 = {}
        dict1['label'] = labels[i]
        dict1['lower_whisker'] = bp['whiskers'][i*2].get_ydata()[1]
        dict1['lower_quartile'] = bp['boxes'][i].get_ydata()[1]
        dict1['median'] = bp['medians'][i].get_ydata()[1]
        dict1['upper_quartile'] = bp['boxes'][i].get_ydata()[2]
        dict1['upper_whisker'] = bp['whiskers'][(i*2)+1].get_ydata()[1]
        rows_list.append(dict1)
    return pd.DataFrame(rows_list)

# =====================================================================================

def visualize_sann(filename="image", title="Neural Network"):
    global sann
    sann.draw(save_to_file=True, filename="img\\salience_gif\\%s.jpg"%(filename), title=title)
    return True

# =====================================================================================

def cls():
    os.system('cls')
    return ""

# =====================================================================================

def scriptTimer(startFlag=False, endFlag=False, startTime=0):
    global time_array
    if startFlag:
        startTime = datetime.now()
        return startTime
    if endFlag and startTime:
        diffTime = datetime.now() - startTime;
        time_array.append(diffTime.microseconds)
        #print ("Time Taken: %s" % diffTime)
        return diffTime

# =====================================================================================

global state, autoencoder, encoder, decoder, sann, x_train, y_train, x_encoded, time_array

request_history = []
time_array = []
autoencoder = None
encoder = None
decoder = None
x_train = None
x_test = None
sann = None

img_dim = 100

state = {
    "input": empty_img(img_dim=img_dim),
    "classification": {
        "key" : 0,
        "label" : 0,
        "confidence" : 0
    },
    "reverse_salience" : 0,
    "prediction": empty_img(img_dim=img_dim),
    "request_load" : 0
}

# =====================================================================================
