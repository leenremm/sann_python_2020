from datetime import datetime
import os
os.system('cls')

import pprint
import numpy as np
pp = pprint.PrettyPrinter(indent=4)
import base64
import copy
import pandas as pd

# =====================================================================================

from functions import *

# =====================================================================================

cls()

# Initialize datasets
load_dataset()

# Load pre-processing autoencoder
load_autoencoder()
evaluate_autoencoder()
log("Step 1a: Initialize pre-processing (autoencoder)")

# Initialize SANN
init_sann()
visualize_sann("001", "Step 1b: SANN initialized (random weights)")

# ==============================================================================

load_sann()

# Comment / uncomment reverse_salience line in functions.py > classify_from_encoding()

for i in range (0,100):
    class_score, individual_score, class_confs, individual_confs = evaluate_sann()
    print ("Epoch: %3d, Class: %d/12, Individual: %d/12, Class confs: %s, Individual confs: %s" % (i, class_score, individual_score, np.round(class_confs,3), np.round(individual_confs,3)))

import pdb; pdb.set_trace()

# =====================================================================================
