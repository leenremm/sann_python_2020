from datetime import datetime
import os
os.system('cls')

import pprint
import numpy as np
pp = pprint.PrettyPrinter(indent=4)
import base64
import copy
import pandas as pd

# =====================================================================================

from functions import *

# =====================================================================================

cls()

# Initialize datasets
load_dataset()

# Load pre-processing autoencoder
load_autoencoder()

# Initialize SANN
init_sann()
load_sann()

class_score, individual_score, class_confs, individual_confs = evaluate_sann(verbose=True)

import pdb; pdb.set_trace()

# =====================================================================================
