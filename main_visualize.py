from datetime import datetime
import os
os.system('cls')

import pprint
import numpy as np
pp = pprint.PrettyPrinter(indent=4)
import base64
import copy
import pandas as pd

# =====================================================================================

from functions import *

# =====================================================================================

cls()

# Initialize datasets
load_dataset()

# ==============================================================================

# Load pre-processing autoencoder
load_autoencoder()
evaluate_autoencoder()
log("Step 1a: Initialize pre-processing (autoencoder)")

# ==============================================================================

# Initialize SANN
init_sann()
visualize_sann("001", "Step 1: SANN initialized (random weights)")

# ==============================================================================

load_sann()
visualize_sann("002", "Step 2: SANN weights loaded from save file")

# ==============================================================================

# Negative Salience Training (dog)
salience_train_sann(image_key=8, salience_value=1)
evaluate_sann()
visualize_sann("003", "Step 3: Negative salience training (image #9, class: dog)")

# ==============================================================================

salience_update_weights_sann(image_key=8)
evaluate_sann()
visualize_sann("004", "Step 4: Weights strengthened proportional to activation")

# ==============================================================================

import pdb; pdb.set_trace()

# =====================================================================================
